﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="9008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Source" Type="Folder">
			<Item Name="MGI Wait" Type="Folder">
				<Item Name="Wait (Double).vi" Type="VI" URL="../Source/MGI Wait/Wait (Double).vi"/>
				<Item Name="Wait (U32).vi" Type="VI" URL="../Source/MGI Wait/Wait (U32).vi"/>
			</Item>
			<Item Name="Milliseconds Since Last Call.vi" Type="VI" URL="../Source/Milliseconds Since Last Call.vi"/>
			<Item Name="Milliseconds Since Last Reset.vi" Type="VI" URL="../Source/Milliseconds Since Last Reset.vi"/>
			<Item Name="Wait.vi" Type="VI" URL="../Source/Wait.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
