#MGI Timing

####This package is implemented with LabVIEW version 9.0

VIs that operate with absolute time, millisecond ticks, or relative time



## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

##Project VIs
###Wait (Double).vi

Waits the specified number of seconds.  Useful to create data dependency on the error lines and to have a smaller icon.  If "Error In" has an error, then this VI won't perform the wait.

![Icon](\images\Wait (Double) Icon.png.png)
![Connector Pane](\images\Wait (Double) Connector Pane.png.png)
Version 9.0



###Wait (U32).vi

Waits the specified number of milliseconds.  Useful to create data dependency on the error lines and to have a smaller icon.  If "Error In" has an error, then this VI won't perform the wait.

![Icon](\images\Wait (U32) Icon.png.png)
![Connector Pane](\images\Wait (U32) Connector Pane.png.png)
Version 9.0



###Milliseconds Since Last Call.vi

This VI stores the tick count on a shift register and provides as an output the number of milliseconds since the last time this VI was called.

![Icon](\images\Milliseconds Since Last Call Icon.png.png)
![Connector Pane](\images\Milliseconds Since Last Call Connector Pane.png.png)
Version 9.0



###Milliseconds Since Last Reset.vi

Returns the amount of time in milliseconds since the last time the VI was reset.  This VI is non-reentrant.

![Icon](\images\Milliseconds Since Last Reset Icon.png.png)
![Connector Pane](\images\Milliseconds Since Last Reset Connector Pane.png.png)
Version 9.0



###Wait.vi

This is the polymorphic version that contains both millisecond and second versions of MGI Wait.  Useful to create data dependency on the error lines and to have a smaller icon.  If "Error In" has an error, then this VI won't perform the wait.

![Icon](\images\Wait Icon.png.png)
![Connector Pane](\images\Wait Connector Pane.png.png)
Version 9.0



